package cn.xlbweb.chat;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: bobi
 * @date: 2019-08-10 20:06
 * @description:
 */
@SpringBootApplication
@Slf4j
public class App implements ApplicationRunner {

    @Value("${server.port}")
    public int port;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Tomcat服务启动完成: http://localhost:{}", port);
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
